/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/404";
exports.ids = ["pages/404"];
exports.modules = {

/***/ "./pages/404.tsx":
/*!***********************!*\
  !*** ./pages/404.tsx ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _component_BazarImage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @component/BazarImage */ \"./src/components/BazarImage.tsx\");\n/* harmony import */ var _component_FlexBox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @component/FlexBox */ \"./src/components/FlexBox.tsx\");\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core */ \"@material-ui/core\");\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/link */ \"./node_modules/next/link.js\");\n/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\nvar _jsxFileName = \"/home/ratul/Downloads/Archive/pages/404.tsx\";\nvar __jsx = (react__WEBPACK_IMPORTED_MODULE_0___default().createElement);\n\n\n\n\n\n\n\nconst Error404 = () => {\n  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_5__.useRouter)();\n\n  const handleGoBack = async () => {\n    router.back();\n  };\n\n  return __jsx(_component_FlexBox__WEBPACK_IMPORTED_MODULE_2__.default, {\n    flexDirection: \"column\",\n    minHeight: \"100vh\",\n    justifyContent: \"center\",\n    alignItems: \"center\",\n    px: 2,\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 16,\n      columnNumber: 5\n    }\n  }, __jsx(_component_BazarImage__WEBPACK_IMPORTED_MODULE_1__.default, {\n    src: \"/assets/images/illustrations/404.svg\",\n    sx: {\n      display: 'block',\n      maxWidth: '320px',\n      width: '100%',\n      mb: '1.5rem'\n    },\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 23,\n      columnNumber: 7\n    }\n  }), __jsx(_component_FlexBox__WEBPACK_IMPORTED_MODULE_2__.default, {\n    flexWrap: \"wrap\",\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 32,\n      columnNumber: 7\n    }\n  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__.Button, {\n    variant: \"outlined\",\n    color: \"primary\",\n    sx: {\n      m: '0.5rem'\n    },\n    onClick: handleGoBack,\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 33,\n      columnNumber: 9\n    }\n  }, \"Go Back\"), __jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {\n    href: \"/\",\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 41,\n      columnNumber: 9\n    }\n  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__.Button, {\n    variant: \"contained\",\n    color: \"primary\",\n    sx: {\n      m: '0.5rem'\n    },\n    __self: undefined,\n    __source: {\n      fileName: _jsxFileName,\n      lineNumber: 42,\n      columnNumber: 11\n    }\n  }, \"Go to Home\"))));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Error404);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9iYXphci8uL3BhZ2VzLzQwNC50c3g/MDVkYyJdLCJuYW1lcyI6WyJFcnJvcjQwNCIsInJvdXRlciIsInVzZVJvdXRlciIsImhhbmRsZUdvQmFjayIsImJhY2siLCJkaXNwbGF5IiwibWF4V2lkdGgiLCJ3aWR0aCIsIm1iIiwibSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxNQUFNQSxRQUFRLEdBQUcsTUFBTTtBQUNyQixRQUFNQyxNQUFNLEdBQUdDLHNEQUFTLEVBQXhCOztBQUVBLFFBQU1DLFlBQVksR0FBRyxZQUFZO0FBQy9CRixVQUFNLENBQUNHLElBQVA7QUFDRCxHQUZEOztBQUlBLFNBQ0UsTUFBQyx1REFBRDtBQUNFLGlCQUFhLEVBQUMsUUFEaEI7QUFFRSxhQUFTLEVBQUMsT0FGWjtBQUdFLGtCQUFjLEVBQUMsUUFIakI7QUFJRSxjQUFVLEVBQUMsUUFKYjtBQUtFLE1BQUUsRUFBRSxDQUxOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FPRSxNQUFDLDBEQUFEO0FBQ0UsT0FBRyxFQUFDLHNDQUROO0FBRUUsTUFBRSxFQUFFO0FBQ0ZDLGFBQU8sRUFBRSxPQURQO0FBRUZDLGNBQVEsRUFBRSxPQUZSO0FBR0ZDLFdBQUssRUFBRSxNQUhMO0FBSUZDLFFBQUUsRUFBRTtBQUpGLEtBRk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQVBGLEVBZ0JFLE1BQUMsdURBQUQ7QUFBUyxZQUFRLEVBQUMsTUFBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMscURBQUQ7QUFDRSxXQUFPLEVBQUMsVUFEVjtBQUVFLFNBQUssRUFBQyxTQUZSO0FBR0UsTUFBRSxFQUFFO0FBQUVDLE9BQUMsRUFBRTtBQUFMLEtBSE47QUFJRSxXQUFPLEVBQUVOLFlBSlg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGLEVBU0UsTUFBQyxrREFBRDtBQUFNLFFBQUksRUFBQyxHQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDRSxNQUFDLHFEQUFEO0FBQVEsV0FBTyxFQUFDLFdBQWhCO0FBQTRCLFNBQUssRUFBQyxTQUFsQztBQUE0QyxNQUFFLEVBQUU7QUFBRU0sT0FBQyxFQUFFO0FBQUwsS0FBaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixDQVRGLENBaEJGLENBREY7QUFrQ0QsQ0F6Q0Q7O0FBMkNBLCtEQUFlVCxRQUFmIiwiZmlsZSI6Ii4vcGFnZXMvNDA0LnRzeC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBCYXphckltYWdlIGZyb20gJ0Bjb21wb25lbnQvQmF6YXJJbWFnZSdcbmltcG9ydCBGbGV4Qm94IGZyb20gJ0Bjb21wb25lbnQvRmxleEJveCdcbmltcG9ydCB7IEJ1dHRvbiB9IGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlJ1xuaW1wb3J0IExpbmsgZnJvbSAnbmV4dC9saW5rJ1xuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXG5cbmNvbnN0IEVycm9yNDA0ID0gKCkgPT4ge1xuICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKVxuXG4gIGNvbnN0IGhhbmRsZUdvQmFjayA9IGFzeW5jICgpID0+IHtcbiAgICByb3V0ZXIuYmFjaygpXG4gIH1cblxuICByZXR1cm4gKFxuICAgIDxGbGV4Qm94XG4gICAgICBmbGV4RGlyZWN0aW9uPVwiY29sdW1uXCJcbiAgICAgIG1pbkhlaWdodD1cIjEwMHZoXCJcbiAgICAgIGp1c3RpZnlDb250ZW50PVwiY2VudGVyXCJcbiAgICAgIGFsaWduSXRlbXM9XCJjZW50ZXJcIlxuICAgICAgcHg9ezJ9XG4gICAgPlxuICAgICAgPEJhemFySW1hZ2VcbiAgICAgICAgc3JjPVwiL2Fzc2V0cy9pbWFnZXMvaWxsdXN0cmF0aW9ucy80MDQuc3ZnXCJcbiAgICAgICAgc3g9e3tcbiAgICAgICAgICBkaXNwbGF5OiAnYmxvY2snLFxuICAgICAgICAgIG1heFdpZHRoOiAnMzIwcHgnLFxuICAgICAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICAgICAgbWI6ICcxLjVyZW0nLFxuICAgICAgICB9fVxuICAgICAgLz5cbiAgICAgIDxGbGV4Qm94IGZsZXhXcmFwPVwid3JhcFwiPlxuICAgICAgICA8QnV0dG9uXG4gICAgICAgICAgdmFyaWFudD1cIm91dGxpbmVkXCJcbiAgICAgICAgICBjb2xvcj1cInByaW1hcnlcIlxuICAgICAgICAgIHN4PXt7IG06ICcwLjVyZW0nIH19XG4gICAgICAgICAgb25DbGljaz17aGFuZGxlR29CYWNrfVxuICAgICAgICA+XG4gICAgICAgICAgR28gQmFja1xuICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgPExpbmsgaHJlZj1cIi9cIj5cbiAgICAgICAgICA8QnV0dG9uIHZhcmlhbnQ9XCJjb250YWluZWRcIiBjb2xvcj1cInByaW1hcnlcIiBzeD17eyBtOiAnMC41cmVtJyB9fT5cbiAgICAgICAgICAgIEdvIHRvIEhvbWVcbiAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgPC9MaW5rPlxuICAgICAgPC9GbGV4Qm94PlxuICAgIDwvRmxleEJveD5cbiAgKVxufVxuXG5leHBvcnQgZGVmYXVsdCBFcnJvcjQwNFxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/404.tsx\n");

/***/ }),

/***/ "@material-ui/core":
/*!************************************!*\
  !*** external "@material-ui/core" ***!
  \************************************/
/***/ (function(module) {

"use strict";
module.exports = require("@material-ui/core");;

/***/ }),

/***/ "@material-ui/system":
/*!**************************************!*\
  !*** external "@material-ui/system" ***!
  \**************************************/
/***/ (function(module) {

"use strict";
module.exports = require("@material-ui/system");;

/***/ }),

/***/ "../next-server/lib/router-context":
/*!**************************************************************!*\
  !*** external "next/dist/next-server/lib/router-context.js" ***!
  \**************************************************************/
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/next-server/lib/router-context.js");;

/***/ }),

/***/ "../next-server/lib/router/utils/get-asset-path-from-route":
/*!**************************************************************************************!*\
  !*** external "next/dist/next-server/lib/router/utils/get-asset-path-from-route.js" ***!
  \**************************************************************************************/
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/next-server/lib/router/utils/get-asset-path-from-route.js");;

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("next/router");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react-is":
/*!***************************!*\
  !*** external "react-is" ***!
  \***************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-is");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = __webpack_require__.X(0, ["vendors-node_modules_next_link_js","src_components_BazarImage_tsx-src_components_FlexBox_tsx"], function() { return __webpack_exec__("./pages/404.tsx"); });
module.exports = __webpack_exports__;

})();