import firebase from "../firebase.config";
const CHANGE_CART_AMOUNT = 'CHANGE_CART_AMOUNT'

export const cartInitialState = {
  cartList: [
    {
      price: 250,
      name: 'Ford 2019',
      imgUrl: '/assets/images/products/Automotive/1.Ford2019.png',
      id: '7222243834583537',
      qty: 1,
    },
    {
      price: 250,
      name: 'Porsche 2020',
      imgUrl: '/assets/images/products/Automotive/28.Porsche2020.png',
      id: '38553442244076086',
      qty: 1,
    },
    {
      price: 250,
      name: 'Heavy 20kt Gold Necklace',
      imgUrl:
        '/assets/images/products/Fashion/Jewellery/9.Heavy20ktGoldNecklace.png',
      id: '9573201630529315',
      qty: 1,
    },
  ],
  currentCartRef: ""
}

export type CartItem = {
  id: string | number
  name: string
  qty: number
  price: number
  imgUrl?: string
}

export type cartStateType = {
  cartList: CartItem[],
  currentCartRef: string
}

export type cartActionType = {
  type: typeof CHANGE_CART_AMOUNT
  payload: CartItem
}

export const cartReducer = (state: cartStateType, action: cartActionType) => {
  switch (action.type) {
    case CHANGE_CART_AMOUNT:
      let cartList = state.cartList
      let cartItem = action.payload
      let tempRef =  state.currentCartRef;
      let haveToReturnRef = false;
      if(tempRef.length === 0){
        haveToReturnRef = true;
        tempRef = firebase.firestore().collection('carts').doc().id;
      } 
      let exist = cartList.find((item) => item.id === cartItem.id)

      if (cartItem.qty < 1){
        let modified = cartList.filter((item) => item.id !== cartItem.id);
        pushCartToFirebase(tempRef, modified);
        return {
          //cartList: cartList.filter((item) => item.id !== cartItem.id),
          cartList: [...modified],
          currentCartRef: haveToReturnRef ? tempRef : state.currentCartRef
        }
      }
        
      else if (exist)
        {
          let modified = cartList.map((item) => {
            if (item.id === cartItem.id) return { ...item, qty: cartItem.qty }
            else return item
          });
          pushCartToFirebase(tempRef, modified);
          return {
          // cartList: cartList.map((item) => {
          //   if (item.id === cartItem.id) return { ...item, qty: cartItem.qty }
          //   else return item
          // }),
          cartList: [...modified],
          currentCartRef: haveToReturnRef ? tempRef : state.currentCartRef
        }}
      else{
        let modified = [...cartList, cartItem]
        pushCartToFirebase(tempRef, modified);
        return {
          cartList: [...modified],
          currentCartRef: haveToReturnRef ? tempRef : state.currentCartRef
        }
      }
        

    default: {
    }
  }
}
const pushCartToFirebase = async (docId: string, cartList: CartItem[]) => {
  console.log(cartList);
  await firebase.firestore().collection("carts").doc(docId).set({data: cartList});
}